/*
    SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef PULSEAUDIOMOBILESETTINGS_H
#define PULSEAUDIOMOBILESETTINGS_H

#include <KQuickAddons/ConfigModule>

#include <PulseAudioQt/Sink>
#include <PulseAudioQt/Source>

class PulseAudioMobileSettings : public KQuickAddons::ConfigModule
{
    Q_OBJECT
    Q_PROPERTY(PulseAudioQt::Sink* sink MEMBER m_sink NOTIFY sinkChanged)
    Q_PROPERTY(PulseAudioQt::Source* source MEMBER m_source NOTIFY sourceChanged)
public:
    PulseAudioMobileSettings(QObject *parent, const QVariantList &args);
    ~PulseAudioMobileSettings() override;

Q_SIGNALS:
    void sinkChanged();
    void sourceChanged();

public Q_SLOTS:
    void defaults() final;
    void load() final;
    void save() final;

private:
    PulseAudioQt::Sink *m_sink = nullptr;
    PulseAudioQt::Source *m_source = nullptr;
};

#endif
