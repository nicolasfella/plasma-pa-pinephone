/*
    SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11

import org.kde.kirigami 2.7 as Kirigami
import org.kde.kcm 1.2

Column {

    enum DeviceType {
        Output,
        Input
    }

    property alias title: titleLabel.text
    property QtObject volumeObject
    property var deviceType

    Label {
        id: titleLabel
    }

    RowLayout {

        width: parent.width

        Button {
            icon.name: deviceType === VolumeSlider.DeviceType.Output ? "audio-volume-high" : "microphone-sensitivity-high"
        }

        Slider {
            id: slider
            Layout.fillWidth: true
            value: volumeObject ? volumeObject.volume : 0
            from: 0
            to: 65536
            onValueChanged: volumeObject.volume = value
        }

        Label {
            text: {
                const percent = Math.round((slider.value / slider.to) * 100)
                return i18n("%1\%", percent)
            }
        }
    }


}
