/*
    SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11

import org.kde.kirigami 2.7 as Kirigami
import org.kde.kcm 1.2

SimpleKCM {

    id: root

    ColumnLayout {

        anchors.fill: root

        VolumeSlider {
            title: i18n("Internal speaker")
            volumeObject: kcm.sink
            Layout.fillWidth: true
            deviceType: VolumeSlider.DeviceType.Output
        }

        Kirigami.Separator {
            Layout.fillWidth: true
        }

        VolumeSlider {
            title: i18n("Internal microphone")
            volumeObject: kcm.source
            Layout.fillWidth: true
            deviceType: VolumeSlider.DeviceType.Input
        }
    }
}
