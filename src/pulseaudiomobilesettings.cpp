/*
    SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
    SPDX-License-Identifier: GPL-2.0-or-later
*/
#include "pulseaudiomobilesettings.h"

#include <KPluginFactory>
#include <KLocalizedString>
#include <KAboutData>

#include <QDebug>

#include <PulseAudioQt/Context>

K_PLUGIN_CLASS_WITH_JSON(PulseAudioMobileSettings, "metadata.json")

PulseAudioMobileSettings::PulseAudioMobileSettings(QObject *parent, const QVariantList &args)
    : KQuickAddons::ConfigModule(parent, args)
{
    KAboutData *aboutData = new KAboutData("kcm_pulseaudio_mobile",
                                           i18nc("@title", "Audio"),
                                           "0.1",
                                           QStringLiteral(""),
                                           KAboutLicense::LicenseKey::GPL_V2,
                                           i18nc("@info:credit", "Copyright 2020 Nicolas Fella"));

    aboutData->addAuthor(i18nc("@info:credit", "Nicolas Fella"),
                        i18nc("@info:credit", "Author"),
                        QStringLiteral("nicolas.fella@gmx.de"));

    setAboutData(aboutData);
    setButtons(Help);
}

PulseAudioMobileSettings::~PulseAudioMobileSettings()
{
}

void PulseAudioMobileSettings::load()
{
    connect(PulseAudioQt::Context::instance(), &PulseAudioQt::Context::sinkAdded, this, [this] {
        m_sink = PulseAudioQt::Context::instance()->sinks()[0];
        Q_EMIT sinkChanged();
    });

    connect(PulseAudioQt::Context::instance(), &PulseAudioQt::Context::sourceAdded, this, [this] {
        m_source = PulseAudioQt::Context::instance()->sources()[0];
        Q_EMIT sourceChanged();
    });
}


void PulseAudioMobileSettings::save()
{
}

void PulseAudioMobileSettings::defaults()
{
}

#include "pulseaudiomobilesettings.moc"
